## Etape pour la mise en place d'un serveur de diff geoserver & geonetwork

### Serveur à but pédagogique de support de cours

**Se connecter en mode CLI à son serveur distant grâce au protocole ssh**

`ssh user@adresse_ip - p port`

### Installation de quelques pré-requis

- installations de bases :

	`sudo apt -y update && sudo apt -y upgrade`

	`sudo apt -y install vim curl htop`

- documentation pour l'install de docker sur Debian 10 : https://sqx-bki.fr/comment-installer-docker-sur-debian-10/

Les dépendances :

	sudo apt -y install apt-transport-https ca-certificates gnupg2 software-properties-common

Import de la signature du dépôt :

	curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

On ajoute les dépôts :

	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
	sudo apt-get update

Installation de docker

	sudo apt-get install docker-ce docker-ce-cli containerd.io

Installation de docker-compose

	sudo curl -L https://github.com/docker/compose/releases/download/1.27.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

	sudo chmod +x /usr/local/bin/docker-compose

### Ecriture du docker-compose.yml
	- les deux application sont basées sur l'instance postgresql qui est installée en même temps : 
	Se connecter au docker db en mode CLI : docker-compose exec db bash
	- geonetwork est accessible : `localhost:8081/geonetwork/`
	- geoserver est accessible  : `localhost:8082/web/`

**Remplacer localhost par l'adresse ip de son réseau**
