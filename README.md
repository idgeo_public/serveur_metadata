# Serveur metadata

Déploiement sur serveur debian d'une solution orientée diffusion de métadonnées grâce à Docker Compose.


- Utiliser la procédure [install.md](https://gitlab.com/idgeo_public/serveur_metadata/-/master/install.md) pour préparer le serveur qui déploiera le docker-compose Geonetwork / PostgreSQL / Geoserver.

- Le fichier [dokcer-compose](https://gitlab.com/idgeo_public/serveur_metadata/-/master/docker-compose.yml) à faire tourner depuis un dossier choisi sur le serveur :

        `scp docker-compose.yml user@ip_adress -p port /home/user/`

        `docker-compose build`

        `docker-compose up -d`